package com.conygre.training.trader.rest;

import java.util.Collection;

import com.conygre.training.trader.model.Trade;
import com.conygre.training.trader.service.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getAllTrades() {
        LOG.debug("getAllTrades() called");
        return tradeService.getAllTrades();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        LOG.debug("addTrade() called : " + trade);
        tradeService.addTrade(trade);
    }
}